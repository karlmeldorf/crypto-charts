import { FastifyPluginAsync } from 'fastify';
import fastifyCors from '@fastify/cors';
import fastifyMultipart from '@fastify/multipart';
import AutoLoad, {AutoloadPluginOptions} from 'fastify-autoload';
import { join } from 'path';

export type AppOptions = {
    // Place your custom options for app below here.
  } & Partial<AutoloadPluginOptions>;

const app: FastifyPluginAsync<AppOptions> = async (fastify, opts): Promise<void> => {  
  void fastify.register(fastifyCors, {
    origin: "*",
    methods: ["GET", "POST"],
  });

  void fastify.register(fastifyMultipart);

  void fastify.register(AutoLoad, {
    dir: join(__dirname, 'routes'),
    options: opts
  })
}

export default app;
export { app }