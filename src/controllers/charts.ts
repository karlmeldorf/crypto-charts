import { FastifyReply, FastifyRequest } from "fastify";
import * as fs from 'fs';
import * as path from 'path';
import { parseStream } from 'fast-csv';
import { generatePdf } from "../utils/pdfGenerator";
import { MAX_NUMBER_OF_ROWS } from "../utils/constants";

interface GetChartByNameRoute {
    Params: {
        name: number
    }
}

export const getChartByName = async (req: FastifyRequest<GetChartByNameRoute>, reply: FastifyReply) => {
    return reply.header('Content-Type', 'application/pdf').send(fs.readFileSync(path.join(__dirname, `../../src/files/${req.params.name}`)));
}

interface CreateChartRoute {
    Body: {
      file: any
    }
}

export const createChart = async (req: FastifyRequest<CreateChartRoute>, reply: FastifyReply) => {
    const data = await req.file();

    if (!data) {
      return reply.send({ success: false });
    }

    const newFileName = data.filename.replace('.csv', '.pdf');
    const fileData: { [key: string]: any }[] = [];
    const labels: string[] = [];
    const values: number[] = [];

    parseStream(data.file)
      .on('data', (rowData) => {
        const date = String(rowData[3].split(' ')[0]);
        const price = Number(rowData[4]);

        if (labels.length < MAX_NUMBER_OF_ROWS && !Number.isNaN(price)) {
          labels.push(date);
          values.push(price);
          fileData.push(rowData);
        }
      })
      .on('end', async () => {
        await generatePdf(newFileName, labels, values)
        
        reply.type('application/json').header("Access-Control-Allow-Origin", "*").send({ success: true, data: { fileName: newFileName } });
      });
}
