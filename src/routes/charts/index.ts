import { FastifyInstance } from "fastify";

import { getChartByName, createChart } from "../../controllers/charts"

const getChartByNameOptions = {
    handler: getChartByName,
    options: {
        schema: {
            params: {  
                name: { type: 'string' },
            },
        }
    } 
}

const createChartOptions = {
    handler: createChart,
    options: {
        payload: {
          maxBytes: 1048576, // 1 MB
          parse: true,
        },
    },
}

export default async function chartsRoutes(app: FastifyInstance): Promise<void> {
    app.get('/charts/:name', getChartByNameOptions);
    app.post('/chart', createChartOptions);
}