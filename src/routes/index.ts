import { FastifyInstance } from 'fastify';

import chartsRoutes from './charts';

const API_BASE_PATH = '/api/v1'

export default async function routes(app: FastifyInstance): Promise<void> {
    await app.register(chartsRoutes, { prefix: API_BASE_PATH });
}