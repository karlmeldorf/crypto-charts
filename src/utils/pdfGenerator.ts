import * as fs from 'fs';
import * as path from 'path';
import { ChartJSNodeCanvas } from 'chartjs-node-canvas';

export async function generatePdf(fileName: string, labels: string[], values: number[]) {
    const chartJSNodeCanvas = new ChartJSNodeCanvas({ type: 'pdf', width: 800, height: 600 });

    const configuration = {
        type: 'line' as const,
        data: {
            labels: labels,
            datasets: [{
                label: 'Price chart',
                data: values,
            }],
        },
    };

    const pdfBuffer = chartJSNodeCanvas.renderToBufferSync(configuration, 'application/pdf');

    fs.writeFileSync(path.join(__dirname, `../../src/files/${fileName}`), pdfBuffer);

}